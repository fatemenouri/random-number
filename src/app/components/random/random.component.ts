import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-random',
  templateUrl: './random.component.html',
  styleUrls: ['./random.component.scss']
})
export class RandomComponent implements OnInit {
  
  constructor() { }
  
  ngOnInit(): void {
  }
  count :number =0; 
  numbersList :number[] =[];

  generateRandomNumber(){
    this.numbersList=[];
    for(let i=0; i< this.count ;i++){
      this.numbersList.push(Math.floor( Math.random()*100));
    }
    this.numbersList.sort((a,b)=>a-b);
  }
}
